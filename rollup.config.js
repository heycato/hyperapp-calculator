import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import serve from 'rollup-plugin-serve'
import livereload from 'rollup-plugin-livereload'
import scss from 'rollup-plugin-scss'

export default {
	input: 'src/main.js',
	output: {
		file: 'public/js/bundle.js',
		format: 'iife'
	},
	plugins: [
		scss({
			output: 'public/css/main.css'
		}),
		resolve(),
		commonjs(),
		serve({
			open: true,
			host: 'localhost',
			port: 8080,
			contentBase: [ 'public' ]
		}),
		livereload()
	]
}
