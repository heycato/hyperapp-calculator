
(function(l, i, v, e) { v = l.createElement(i); v.async = 1; v.src = '//' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; e = l.getElementsByTagName(i)[0]; e.parentNode.insertBefore(v, e)})(document, 'script');
(function () {
  'use strict';

  function h(name, attributes) {
    var rest = [];
    var children = [];
    var length = arguments.length;

    while (length-- > 2) rest.push(arguments[length]);

    while (rest.length) {
      var node = rest.pop();
      if (node && node.pop) {
        for (length = node.length; length--; ) {
          rest.push(node[length]);
        }
      } else if (node != null && node !== true && node !== false) {
        children.push(node);
      }
    }

    return typeof name === "function"
      ? name(attributes || {}, children)
      : {
          nodeName: name,
          attributes: attributes || {},
          children: children,
          key: attributes && attributes.key
        }
  }

  function app(state, actions, view, container) {
    var map = [].map;
    var rootElement = (container && container.children[0]) || null;
    var oldNode = rootElement && recycleElement(rootElement);
    var lifecycle = [];
    var skipRender;
    var isRecycling = true;
    var globalState = clone(state);
    var wiredActions = wireStateToActions([], globalState, clone(actions));

    scheduleRender();

    return wiredActions

    function recycleElement(element) {
      return {
        nodeName: element.nodeName.toLowerCase(),
        attributes: {},
        children: map.call(element.childNodes, function(element) {
          return element.nodeType === 3 // Node.TEXT_NODE
            ? element.nodeValue
            : recycleElement(element)
        })
      }
    }

    function resolveNode(node) {
      return typeof node === "function"
        ? resolveNode(node(globalState, wiredActions))
        : node != null
          ? node
          : ""
    }

    function render() {
      skipRender = !skipRender;

      var node = resolveNode(view);

      if (container && !skipRender) {
        rootElement = patch(container, rootElement, oldNode, (oldNode = node));
      }

      isRecycling = false;

      while (lifecycle.length) lifecycle.pop()();
    }

    function scheduleRender() {
      if (!skipRender) {
        skipRender = true;
        setTimeout(render);
      }
    }

    function clone(target, source) {
      var out = {};

      for (var i in target) out[i] = target[i];
      for (var i in source) out[i] = source[i];

      return out
    }

    function setPartialState(path, value, source) {
      var target = {};
      if (path.length) {
        target[path[0]] =
          path.length > 1
            ? setPartialState(path.slice(1), value, source[path[0]])
            : value;
        return clone(source, target)
      }
      return value
    }

    function getPartialState(path, source) {
      var i = 0;
      while (i < path.length) {
        source = source[path[i++]];
      }
      return source
    }

    function wireStateToActions(path, state, actions) {
      for (var key in actions) {
        typeof actions[key] === "function"
          ? (function(key, action) {
              actions[key] = function(data) {
                var result = action(data);

                if (typeof result === "function") {
                  result = result(getPartialState(path, globalState), actions);
                }

                if (
                  result &&
                  result !== (state = getPartialState(path, globalState)) &&
                  !result.then // !isPromise
                ) {
                  scheduleRender(
                    (globalState = setPartialState(
                      path,
                      clone(state, result),
                      globalState
                    ))
                  );
                }

                return result
              };
            })(key, actions[key])
          : wireStateToActions(
              path.concat(key),
              (state[key] = clone(state[key])),
              (actions[key] = clone(actions[key]))
            );
      }

      return actions
    }

    function getKey(node) {
      return node ? node.key : null
    }

    function eventListener(event) {
      return event.currentTarget.events[event.type](event)
    }

    function updateAttribute(element, name, value, oldValue, isSvg) {
      if (name === "key") ; else if (name === "style") {
        if (typeof value === "string") {
          element.style.cssText = value;
        } else {
          if (typeof oldValue === "string") oldValue = element.style.cssText = "";
          for (var i in clone(oldValue, value)) {
            var style = value == null || value[i] == null ? "" : value[i];
            if (i[0] === "-") {
              element.style.setProperty(i, style);
            } else {
              element.style[i] = style;
            }
          }
        }
      } else {
        if (name[0] === "o" && name[1] === "n") {
          name = name.slice(2);

          if (element.events) {
            if (!oldValue) oldValue = element.events[name];
          } else {
            element.events = {};
          }

          element.events[name] = value;

          if (value) {
            if (!oldValue) {
              element.addEventListener(name, eventListener);
            }
          } else {
            element.removeEventListener(name, eventListener);
          }
        } else if (
          name in element &&
          name !== "list" &&
          name !== "type" &&
          name !== "draggable" &&
          name !== "spellcheck" &&
          name !== "translate" &&
          !isSvg
        ) {
          element[name] = value == null ? "" : value;
        } else if (value != null && value !== false) {
          element.setAttribute(name, value);
        }

        if (value == null || value === false) {
          element.removeAttribute(name);
        }
      }
    }

    function createElement(node, isSvg) {
      var element =
        typeof node === "string" || typeof node === "number"
          ? document.createTextNode(node)
          : (isSvg = isSvg || node.nodeName === "svg")
            ? document.createElementNS(
                "http://www.w3.org/2000/svg",
                node.nodeName
              )
            : document.createElement(node.nodeName);

      var attributes = node.attributes;
      if (attributes) {
        if (attributes.oncreate) {
          lifecycle.push(function() {
            attributes.oncreate(element);
          });
        }

        for (var i = 0; i < node.children.length; i++) {
          element.appendChild(
            createElement(
              (node.children[i] = resolveNode(node.children[i])),
              isSvg
            )
          );
        }

        for (var name in attributes) {
          updateAttribute(element, name, attributes[name], null, isSvg);
        }
      }

      return element
    }

    function updateElement(element, oldAttributes, attributes, isSvg) {
      for (var name in clone(oldAttributes, attributes)) {
        if (
          attributes[name] !==
          (name === "value" || name === "checked"
            ? element[name]
            : oldAttributes[name])
        ) {
          updateAttribute(
            element,
            name,
            attributes[name],
            oldAttributes[name],
            isSvg
          );
        }
      }

      var cb = isRecycling ? attributes.oncreate : attributes.onupdate;
      if (cb) {
        lifecycle.push(function() {
          cb(element, oldAttributes);
        });
      }
    }

    function removeChildren(element, node) {
      var attributes = node.attributes;
      if (attributes) {
        for (var i = 0; i < node.children.length; i++) {
          removeChildren(element.childNodes[i], node.children[i]);
        }

        if (attributes.ondestroy) {
          attributes.ondestroy(element);
        }
      }
      return element
    }

    function removeElement(parent, element, node) {
      function done() {
        parent.removeChild(removeChildren(element, node));
      }

      var cb = node.attributes && node.attributes.onremove;
      if (cb) {
        cb(element, done);
      } else {
        done();
      }
    }

    function patch(parent, element, oldNode, node, isSvg) {
      if (node === oldNode) ; else if (oldNode == null || oldNode.nodeName !== node.nodeName) {
        var newElement = createElement(node, isSvg);
        parent.insertBefore(newElement, element);

        if (oldNode != null) {
          removeElement(parent, element, oldNode);
        }

        element = newElement;
      } else if (oldNode.nodeName == null) {
        element.nodeValue = node;
      } else {
        updateElement(
          element,
          oldNode.attributes,
          node.attributes,
          (isSvg = isSvg || node.nodeName === "svg")
        );

        var oldKeyed = {};
        var newKeyed = {};
        var oldElements = [];
        var oldChildren = oldNode.children;
        var children = node.children;

        for (var i = 0; i < oldChildren.length; i++) {
          oldElements[i] = element.childNodes[i];

          var oldKey = getKey(oldChildren[i]);
          if (oldKey != null) {
            oldKeyed[oldKey] = [oldElements[i], oldChildren[i]];
          }
        }

        var i = 0;
        var k = 0;

        while (k < children.length) {
          var oldKey = getKey(oldChildren[i]);
          var newKey = getKey((children[k] = resolveNode(children[k])));

          if (newKeyed[oldKey]) {
            i++;
            continue
          }

          if (newKey != null && newKey === getKey(oldChildren[i + 1])) {
            if (oldKey == null) {
              removeElement(element, oldElements[i], oldChildren[i]);
            }
            i++;
            continue
          }

          if (newKey == null || isRecycling) {
            if (oldKey == null) {
              patch(element, oldElements[i], oldChildren[i], children[k], isSvg);
              k++;
            }
            i++;
          } else {
            var keyedNode = oldKeyed[newKey] || [];

            if (oldKey === newKey) {
              patch(element, keyedNode[0], keyedNode[1], children[k], isSvg);
              i++;
            } else if (keyedNode[0]) {
              patch(
                element,
                element.insertBefore(keyedNode[0], oldElements[i]),
                keyedNode[1],
                children[k],
                isSvg
              );
            } else {
              patch(element, oldElements[i], null, children[k], isSvg);
            }

            newKeyed[newKey] = children[k];
            k++;
          }
        }

        while (i < oldChildren.length) {
          if (getKey(oldChildren[i]) == null) {
            removeElement(element, oldElements[i], oldChildren[i]);
          }
          i++;
        }

        for (var i in oldKeyed) {
          if (!newKeyed[i]) {
            removeElement(element, oldKeyed[i][0], oldKeyed[i][1]);
          }
        }
      }
      return element
    }
  }

  const createButton = (state, actions) => ({ label, style, actionType }) =>
  	h('div', {
  		id: `calculator-button-${ label }`,
  		class: `calculator-button ${ style }` ,
  		onclick: (e) => actions[actionType](label)
  	}, label);

  const state = {
  	model: {
  		buttons: [
  			{ label: 'x↔y', style: 'rectangle lt', actionType: 'exchangeRegisters' },
  			{ label: 'R↓', style: 'rectangle lt', actionType: 'rollDownStack' },
  			{ label: 'R↑', style: 'rectangle lt', actionType: 'rollUpStack' },
  			{ label: 'drop', style: 'rectangle lt', actionType: 'dropFromStack' },
  			{ label: 'C', style: 'square drk', actionType: 'clearStage' },
  			{ label: '±', style: 'square drk', actionType: 'negateValue' },
  			{ label: '%', style: 'square drk', actionType: 'valueToPercent' },
  			{ label: '÷', style: 'square hlt', actionType: 'applyDivision' },
  			{ label: '7', style: 'square md', actionType: 'appendToStage' },
  			{ label: '8', style: 'square md', actionType: 'appendToStage' },
  			{ label: '9', style: 'square md', actionType: 'appendToStage' },
  			{ label: '×', style: 'square hlt', actionType: 'applyMultiplication' },
  			{ label: '4', style: 'square md', actionType: 'appendToStage' },
  			{ label: '5', style: 'square md', actionType: 'appendToStage' },
  			{ label: '6', style: 'square md', actionType: 'appendToStage' },
  			{ label: '-', style: 'square hlt', actionType: 'applySubtraction' },
  			{ label: '1', style: 'square md', actionType: 'appendToStage' },
  			{ label: '2', style: 'square md', actionType: 'appendToStage' },
  			{ label: '3', style: 'square md', actionType: 'appendToStage' },
  			{ label: '+', style: 'square hlt', actionType: 'applyAddition' },
  			{ label: '0', style: 'lrg-rectangle md rnd-bl', actionType: 'appendToStage' },
  			{ label: '.', style: 'square md', actionType: 'appendToStage' },
  			{ label: 'enter', style: 'square hlt rnd-br', actionType: 'applyToStack' }
  		]
  	},
  	stack: [],
  	stackLast: '',
  	stackSecond: '',
  	stageIsCalced: false,
  	stageIsDuplicated: false,
  	stage: ''
  };

  const orDefault = (fn, def) => (...args) =>
  	(fn(...args) || def);

  const fromLast = (xs, n) => xs[xs.length - n];

  const last = xs =>
  	fromLast(xs, 1);

  const getLast = orDefault(last, '');

  const secondFromLast = xs =>
  	fromLast(xs, 2);

  const getSecond = orDefault(secondFromLast, '');

  const thirdFromLast = xs =>
  	fromLast(xs, 3);

  const getThird = orDefault(thirdFromLast, '');

  const actions = {
  	appendToStage: x => state => {
  		let output;
  		if(state.stageIsCalced) {
  			output = {
  				stage: x,
  				stackLast: Number(state.stage),
  				stackSecond: getLast(state.stack),
  				stack: state.stack.concat(state.stage),
  				stageIsCalced: false
  			};
  		} else if(state.stageIsDuplicated) {
  			output = {
  				stage: x,
  				stageIsDuplicated: false
  			};
  		} else {
  			output = {
  				stage: state.stage + x
  			};
  		}
  		return output
  	},
  	applyToStack: () => state => ({
  		stack: state.stack.concat(Number(state.stage)),
  		stackLast: Number(state.stage),
  		stackSecond: getLast(state.stack),
  		stageIsDuplicated: true
  	}),
  	applyAddition: () => state => ({
  		stageIsCalced: true,
  		stage: Number(state.stack.pop()) + Number(state.stage),
  		stackLast: getLast(state.stack),
  		stackSecond: getSecond(state.stack)
  	}),
  	applySubtraction: () => state => ({
  		stageIsCalced: true,
  		stage: Number(state.stack.pop()) - Number(state.stage),
  		stackLast: getLast(state.stack),
  		stackSecond: getSecond(state.stack)
  	}),
  	applyMultiplication: () => state => ({
  		stageIsCalced: true,
  		stage: Number(state.stack.pop()) * Number(state.stage),
  		stackLast: getLast(state.stack),
  		stackSecond: getSecond(state.stack)
  	}),
  	applyDivision: () => state => ({
  		stageIsCalced: true,
  		stage: Number(state.stack.pop()) / Number(state.stage),
  		stackLast: getLast(state.stack),
  		stackSecond: getSecond(state.stack)
  	}),
  	clearStage: () => state => ({
  		stage: 0
  	}),
  	dropFromStack: () => state => ({
  		stage: state.stack.pop(),
  		stackLast: getLast(state.stack),
  		stackSecond: getSecond(state.stack),
  		stageIsDuplicated: true
  	}),
  	exchangeRegisters: () => state => {
  		console.log('implement exchange registers');
  	},
  	negateValue: () => state => ({
  		stage: Number(-state.stage)
  	}),
  	rollDownStack: () => state => {
  		if(!isNaN(state.stage) && !state.stateIsCalced && !state.stageIsDuplicated) {
  			state.stack.push(Number(state.stage));
  		}
  		const first = state.stack.shift();
  		state.stack.push(first);
  		return {
  			stage: getLast(state.stack),
  			stack: state.stack,
  			stackLast: getSecond(state.stack),
  			stackSecond: getThird(state.stack),
  			stageIsDuplicated: true
  		}
  	},
  	rollUpStack: () => state => {
  		if(!isNaN(state.stage) && !state.stateIsCalced && !state.stageIsDuplicated) {
  			state.stack.pop(Number(state.stage));
  		}
  		const last = state.stack.pop();
  		state.stack.unshift(last);
  		return {
  			stage: getLast(state.stack),
  			stack: state.stack,
  			stackLast: getSecond(state.stack),
  			stackSecond: getThird(state.stack),
  			stageIsDuplicated: true
  		}
  	},
  	valueToPercent: () => state => ({
  		stage: Number(state.stage) / 100
  	})
  };

  const view = (state, actions) =>
  	h('div', { key: 'body', id: 'calculator-body' },
  		h('div', { key: 'display', id: 'calculator-display' },
  			h('div', { key: 'stack-second', class: 'display-line' }, state.stackSecond),
  			h('div', { key: 'stack-last', class: 'display-line' }, state.stackLast),
  			h('div', { key: 'stage', class: 'display-line stage' }, state.stage)
  		),
  		h('div', { key: 'keypad', id: 'calculator-keypad' },
  			state.model.buttons.map(createButton(state, actions))
  		)
  	);

  window.onload = () =>
  	app(state, actions, view, document.body);

}());
