import { app, h } from 'hyperapp'
import { sum, diff, prod, quot } from './calc-fns.js'
import './calculator.css'

const createButton = (state, actions) => ({ label, style, actionType }) =>
	h('div', {
		id: `calculator-button-${ label }`,
		class: `calculator-button ${ style }` ,
		onclick: (e) => actions[actionType](label)
	}, label)

const state = {
	model: {
		buttons: [
			{ label: 'x↔y', style: 'rectangle lt', actionType: 'exchangeRegisters' },
			{ label: 'R↓', style: 'rectangle lt', actionType: 'rollDownStack' },
			{ label: 'R↑', style: 'rectangle lt', actionType: 'rollUpStack' },
			{ label: 'drop', style: 'rectangle lt', actionType: 'dropFromStack' },
			{ label: 'C', style: 'square drk', actionType: 'clearStage' },
			{ label: '±', style: 'square drk', actionType: 'negateValue' },
			{ label: '%', style: 'square drk', actionType: 'valueToPercent' },
			{ label: '÷', style: 'square hlt', actionType: 'applyDivision' },
			{ label: '7', style: 'square md', actionType: 'appendToStage' },
			{ label: '8', style: 'square md', actionType: 'appendToStage' },
			{ label: '9', style: 'square md', actionType: 'appendToStage' },
			{ label: '×', style: 'square hlt', actionType: 'applyMultiplication' },
			{ label: '4', style: 'square md', actionType: 'appendToStage' },
			{ label: '5', style: 'square md', actionType: 'appendToStage' },
			{ label: '6', style: 'square md', actionType: 'appendToStage' },
			{ label: '-', style: 'square hlt', actionType: 'applySubtraction' },
			{ label: '1', style: 'square md', actionType: 'appendToStage' },
			{ label: '2', style: 'square md', actionType: 'appendToStage' },
			{ label: '3', style: 'square md', actionType: 'appendToStage' },
			{ label: '+', style: 'square hlt', actionType: 'applyAddition' },
			{ label: '0', style: 'lrg-rectangle md rnd-bl', actionType: 'appendToStage' },
			{ label: '.', style: 'square md', actionType: 'appendToStage' },
			{ label: 'enter', style: 'square hlt rnd-br', actionType: 'applyToStack' }
		]
	},
	stack: [],
	stackLast: '',
	stackSecond: '',
	stageIsCalced: false,
	stageIsDuplicated: false,
	stage: ''
}

const orDefault = (fn, def) => (...args) =>
	(fn(...args) || def)

const fromLast = (xs, n) => xs[xs.length - n]

const last = xs =>
	fromLast(xs, 1)

const getLast = orDefault(last, '')

const secondFromLast = xs =>
	fromLast(xs, 2)

const getSecond = orDefault(secondFromLast, '')

const thirdFromLast = xs =>
	fromLast(xs, 3)

const getThird = orDefault(thirdFromLast, '')

const log = x => (console.log(x), x)

const actions = {
	appendToStage: x => state => {
		let output
		if(state.stageIsCalced) {
			output = {
				stage: x,
				stackLast: Number(state.stage),
				stackSecond: getLast(state.stack),
				stack: state.stack.concat(state.stage),
				stageIsCalced: false
			}
		} else if(state.stageIsDuplicated) {
			output = {
				stage: x,
				stageIsDuplicated: false
			}
		} else {
			output = {
				stage: state.stage + x
			}
		}
		return output
	},
	applyToStack: () => state => ({
		stack: state.stack.concat(Number(state.stage)),
		stackLast: Number(state.stage),
		stackSecond: getLast(state.stack),
		stageIsDuplicated: true
	}),
	applyAddition: () => state => ({
		stageIsCalced: true,
		stage: Number(state.stack.pop()) + Number(state.stage),
		stackLast: getLast(state.stack),
		stackSecond: getSecond(state.stack)
	}),
	applySubtraction: () => state => ({
		stageIsCalced: true,
		stage: Number(state.stack.pop()) - Number(state.stage),
		stackLast: getLast(state.stack),
		stackSecond: getSecond(state.stack)
	}),
	applyMultiplication: () => state => ({
		stageIsCalced: true,
		stage: Number(state.stack.pop()) * Number(state.stage),
		stackLast: getLast(state.stack),
		stackSecond: getSecond(state.stack)
	}),
	applyDivision: () => state => ({
		stageIsCalced: true,
		stage: Number(state.stack.pop()) / Number(state.stage),
		stackLast: getLast(state.stack),
		stackSecond: getSecond(state.stack)
	}),
	clearStage: () => state => ({
		stage: 0
	}),
	dropFromStack: () => state => ({
		stage: state.stack.pop(),
		stackLast: getLast(state.stack),
		stackSecond: getSecond(state.stack),
		stageIsDuplicated: true
	}),
	exchangeRegisters: () => state => {
		console.log('implement exchange registers')
	},
	negateValue: () => state => ({
		stage: Number(-state.stage)
	}),
	rollDownStack: () => state => {
		if(!isNaN(state.stage) && !state.stateIsCalced && !state.stageIsDuplicated) {
			state.stack.push(Number(state.stage))
		}
		const first = state.stack.shift()
		state.stack.push(first)
		return {
			stage: getLast(state.stack),
			stack: state.stack,
			stackLast: getSecond(state.stack),
			stackSecond: getThird(state.stack),
			stageIsDuplicated: true
		}
	},
	rollUpStack: () => state => {
		if(!isNaN(state.stage) && !state.stateIsCalced && !state.stageIsDuplicated) {
			state.stack.pop(Number(state.stage))
		}
		const last = state.stack.pop()
		state.stack.unshift(last)
		return {
			stage: getLast(state.stack),
			stack: state.stack,
			stackLast: getSecond(state.stack),
			stackSecond: getThird(state.stack),
			stageIsDuplicated: true
		}
	},
	valueToPercent: () => state => ({
		stage: Number(state.stage) / 100
	})
}

const view = (state, actions) =>
	h('div', { key: 'body', id: 'calculator-body' },
		h('div', { key: 'display', id: 'calculator-display' },
			h('div', { key: 'stack-second', class: 'display-line' }, state.stackSecond),
			h('div', { key: 'stack-last', class: 'display-line' }, state.stackLast),
			h('div', { key: 'stage', class: 'display-line stage' }, state.stage)
		),
		h('div', { key: 'keypad', id: 'calculator-keypad' },
			state.model.buttons.map(createButton(state, actions))
		)
	)

window.onload = () =>
	app(state, actions, view, document.body)
